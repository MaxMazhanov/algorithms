// Дан массив слов, найти среди них анаграммы и сгруппировать
// ['сон', 'нос', 'сорт', 'трос', 'торт', 'рост'] -> [[ 'сон', 'нос' ], [ 'сорт', 'трос', 'рост' ], [ 'торт' ]]

function groupAnagrams(arr) {
    const modelToWords = {};
    for (let word of arr) {
        const model = getModel(word);
        if (modelToWords.hasOwnProperty(model)) {
            modelToWords[model].push(word);
        } else {
            modelToWords[model] = [word];
        }
    }
    return Object.values(modelToWords);
}

function getModel(word) {
    return word.split('').sort().join('');
}

console.log(groupAnagrams(['сон', 'нос', 'сорт', 'трос', 'торт', 'рост']));
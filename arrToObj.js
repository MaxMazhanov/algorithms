// На входе массив
const arr = [
    { name: 'width', value: 10 },
    { name: 'height', value: 20 }
];
// На выходе объект {width: 10, height: 20}

const obj = arr.reduce((obj, { name, value }) => ({ ...obj, [name]: value }), {});

console.log(obj);
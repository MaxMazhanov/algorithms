// Массив неповторяющихся чисел, свернуть в диапазоны 
// [1,4,5,2,3,9,8,11,0] => "0-5,8-9,11"
// [1,4,3,2] => "1-4"

const compress = arr => {
    const list = arr.sort((a, b) => a - b);
    let result = '';
    for (let i = 0; i < list.length; i++) {
        if (i === list.length - 1) {
            result += list[i];
            break;
        }
        let nextIndex = i;
        for (let j = i + 1; j < list.length; j++) {
            if (list[j] - list[nextIndex] === 1 || list[j] - list[nextIndex] === 0) {
                nextIndex = j;
            } else {
                break;
            }
        }
        if (nextIndex !== i && list[nextIndex]!==list[i]) {
            result += `${list[i]}-${list[nextIndex]}${nextIndex === list.length-1 ? '' : ','}`
        } else {
            result += `${list[i]}${nextIndex === list.length-1 ? '' : ','}`
        }

        i = nextIndex;
    }

    return result;
}

// Сортировка + проход

console.log(compress([1, 4, 5, 2, 3, 9, 8, 11, 0]));
console.log(compress([2, 2]));
console.log(compress([1, 4]));
// Написать каррирование функции sum
function sum(a, b, c) {
    return a + b + c;
}

const curriedSum = curry(sum);

console.log(curriedSum(1, 2, 3)); // 6
console.log(curriedSum(1, 2)(3)); // 6
console.log(curriedSum(1)(2)(3)); // 6

function curry(func, ...curriedArgs) {
    return (...args) => {
        if (curriedArgs.length + args.length >= func.length) {
            return func.call(this, ...curriedArgs, ...args);
        }
        return curry.call(this, func, ...curriedArgs, ...args);
    }
}

// Можно на ES5 через apply, но так лаконичнее
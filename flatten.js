/**
 * flatten
 * Дан массив, в котором могут храниться любые типы данных.
 * Нужно реализовать функцию, которая разворачивает вложенные массивы в исходный массив. 
 * Данные остальных типов должны остаться без изменений.
 * Решение должно учитывать любую вложенность элементов (т.е. не должно содержать рекурсивные вызовы)
 */

// flatten([1, 'any [complex] string', null, function () { },
//     [1, 2, [3, '4'], 0],
//     [], {
//         a: 1
//     }
// ]);
// возвращает [1, 'any [complex] string', null, function() {}, 1, 2, 3, '4', 0, { a: 1 }]

// Красиво, но не эффективно
const flatten1 = list => {
    const buffer = [...list];
    for (let i = 0; i < buffer.length; i++) {
        if (Array.isArray(buffer[i])) {
            buffer.splice(i, 1, ...buffer[i]);
            i--;
        }
    }
    return buffer;
}

const flatten2 = list => {
    const arr = [...list];
    const result = [];

    let elem = arr.pop();
    while (elem !== undefined) {
        if (Array.isArray(elem)) {
            arr.push.call(arr, ...elem)
        } else {
            result.push(elem)
        }
        elem = arr.pop();
    }

    return result.reverse();
}

console.log(flatten2([1, 'any [complex] string', null, function () { }, [1, 2, [3, '4'], 0], [], { a: 1 }]));
const test = [2, 3, 7, 4, 6, 1, 5, 8, 9];

function sortOdds(arr) {
    const buffer = [];

    for (let i of arr) {
        if (i % 2 !== 0) {
            buffer.push(i);
        }
    }

    buffer.sort((a, b) => b - a);

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] % 2 !== 0) {
            arr[i] = buffer.pop();
        }
    }

    return arr;
}

console.log(sortOdds(test));
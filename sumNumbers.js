// Необходимо написать функцию для подсчета суммы всех числовых значений в массиве.
// Массив может содержать любые типы данных, быть не плоским.

// Хак через toString()
function sum1(arr) {
    return arr
        .toString()
        .split(',')
        .reduce((acc, cur) => {
            let n = parseInt(cur);
            n = isNaN(n) ? 0 : n;
            return acc + n;
        }, 0);
}

// Через рекурсию и reduce
function sum2(arr) {
    return arr.reduce(function (result, item) {
        return result += Array.isArray(item) ? sum2(item) : (parseInt(item) || 0);
    }, 0);
}

console.log(sum2([1, 'x', '2x', ['3', ['x2', '5']]])); // => 11
// Необходимо реализовать функцию, которая переставляет все нули в массиве в конец, сохраняя последовательность остальных значений.
// Функция должна работать за O(N) по времени и O(1) по памяти.

const test = [0, 10, 0, 1, 2, 3, 0, 0, 4, 0];

function zeroesToTheEnd(arr) {
    if (arr.length < 2) {
        return arr;
    }
    for (let i = 0; i < arr.length; i++) {
        let flag = true;
        if (arr[i] === 0) {
            for (let j = i + 1; j < arr.length; j++) {
                if (arr[j] !== 0) {
                    arr[i] = arr[j];
                    arr[j] = 0;
                    break;
                }
                if (j === arr.length - 1) {
                    flag = false;
                }
            }
        }
        if (!flag) {
            break;
        }
    }
    return arr;
}

console.log(zeroesToTheEnd(test));